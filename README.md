Your satisfaction with the hearing solution we recommend for you is of utmost importance to us. We know that the more you understand about your level of hearing loss and the solutions that are available to you, the greater the chances are that you will be completely satisfied with your investment.

Address: 2005 Midway Rd, #101, Menasha, WI 54952

Phone: 920-969-1768
